//  ================================================================================
//  ||                                                                            ||
//  ||              esPIC                                                         ||
//  ||              ------------------------------------------------------        ||
//  ||              E L E C T R O S T I C   P A R T I C L E - I N - C E L L       ||
//  ||                                                                            ||
//  ||              D E M O N S T R A T I O N   C O D E                           ||
//  ||              ------------------------------------------------------        ||
//  ||                                                                            ||
//  ||       Developed by: Scott R. Runnels, Ph.D.                                ||
//  ||                     University of Colorado Boulder                         ||
//  ||                                                                            ||
//  ||                For: CU Boulder CSCI 4576/5576 and associated labs          ||
//  ||                                                                            ||
//  ||           Copyright 2020 Scott Runnels                                     ||
//  ||                                                                            ||
//  ||                     Not for distribution or use outside of the             ||
//  ||                     this course.                                           ||
//  ||                                                                            ||
//  ================================================================================

class particles
{

 public:
  int n;
  double fluxTimer;
  double Qp;
  double previousInjectionTime;

  double * x;
  double * y;
  double * vx;
  double * vy;
  double * xf;
  double * yf;
  int    * active;

  // ==
  // ||
  // ||  Constructor
  // ||
  // ==

  particles(int _n)
    {
      n  = _n;

      x      = Array1D_double(n+1);
      y      = Array1D_double(n+1);
      vx     = Array1D_double(n+1);
      vy     = Array1D_double(n+1);
      xf     = Array1D_double(n+1);
      yf     = Array1D_double(n+1);
      active = Array1D_int(n+1);

      for ( int i = 1 ; i < n + 1 ; ++i ) active[i] = 0;
      fluxTimer = 0.;

      Qp = 1.; // Extensive physical quantity represented by each particle
      previousInjectionTime = 0.;
    }

  // ==
  // ||
  // ||  Adds new particles to the list of particles
  // ||
  // ==

    void add(double * xAdd, double * yAdd, double * vxAdd, double *vyAdd, int Np)
      {

        // Add new particles in spaces left inactive (active[j] != 1)
        // to the existing arrays.

        int count = 0;

        for ( int j = 1 ; j <= n ; ++j )
          {
            if ( active[j] == 0 )
              { 

                ++count;
                
                if ( count > Np )
                  {
                    return;
                  }
                
                x[j]      = xAdd[count];
                y[j]      = yAdd[count];
                vx[j]     = vxAdd[count];
                vy[j]     = vyAdd[count];
                active[j] = 1;

              }
          }
        
        // If we are out of space in the existing arrays, resize
        // them and try again.

        FatalError("More room needed for particles. Adjust your -nPtcl value on the command line.");

      }


  // ==
  // ||
  // ||  Moves particles the (provided) dt
  // ||
  // ==

    void move(double dt)
      {
        for ( int i = 1 ; i < n ; ++i )
          {
            if ( active[i] == 1 )
              {

                // Apply force to update velocity with acceleration

                vx[i] += dt * xf[i];
                vy[i] += dt * yf[i];

                // Apply velocity to update position
                
                x [i] += dt * vx[i];
                y [i] += dt * vy[i];
              }
          }
      }

  void addFlux(double time, double y0 , double y1 , double density, double vx_bdy, double vy_bdy)
    {

      double Area     = y1 - y0;                      // Flux area
      double Interval = 1. / (vx_bdy*density*Area);   // Particle interval

      // See if it is time to inject new particles.  Assume for the moment that it is
      // time, in fact, it is past time to inject new particles (time steps might not be
      // just right):
      
      double howLateWeAre = (time - previousInjectionTime) - Interval;

      // If it's not time yet, then were early (i.e., negative late-ness):      

      if ( howLateWeAre < 0. ) return;

      // So we are late!  Calculate how far into the mesh the particles have traveled
      // since entering.

      double distanceIn = howLateWeAre * vx_bdy;

      // Calculate how many particles have crossed the cross section

      double Np = density*Area/Qp ;

      // Calculate the vertical spacing of the particles

      double dy = (y1-y0) / Np;  
                                 
      // Allocate memory to store the new particles

      double * xAdd;
      double * yAdd;
      double * vxAdd;
      double * vyAdd;

      int size_add = int(Np + 1);

      xAdd  = Array1D_double(size_add);
      yAdd  = Array1D_double(size_add);
      vxAdd = Array1D_double(size_add);
      vyAdd = Array1D_double(size_add);

      // Compute their initial position

      for ( int i = 1; i <=  Np ; ++i )
        {
          xAdd[i] = 0.;
          yAdd[i] = y0 + dy/2. + dy*(i-1);
        }

      // Store their initial velocity

      for ( int i = 1 ; i <= Np ; ++i )
        {
          vxAdd[i] = vx_bdy;
          vyAdd[i] = vy_bdy;
        }
      
      // Record the time that this set of particles entered the mesh.  Note that
      // we are recording the actual time, which is the current time minus how
      // late we are.

      previousInjectionTime = time - howLateWeAre;

      // Add the particles

      add(xAdd , yAdd, vxAdd, vyAdd, int(Np));
      
    }

#include "particle_plotter.h"
#include "arrays.h"

};


